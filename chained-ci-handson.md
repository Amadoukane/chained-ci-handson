# Goal: Build a first chain using Chained-CI

Git CI tools allow to launch automatic CI/CD actions for own git project. Chained-CI offers the possibility to chain the CI/CD actions of different git projects.  
The following exercises will show how to structure the projects and execute a complete chaine composed of different gitlab-CI projects.

At the end of the training you should be more familiar with the following
topics:

- [ ] tooling
  - [ ] Gitlab-CI
  - [ ] Chained-CI

## Pre-requisite

Before running the different exercises, make sure to have a Gitlab.com account and request membership (with `Developer` role) on the following projects:

- [chained-ci-handson](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-handson/)
- [chained-ci-dummy1](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-dummy1)
- [chained-ci-dummy2](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-dummy2)

Clone each of these projects in your local dev environment and launch the following commands in order to update the submodules:

```Shell
   git clone git@gitlab.com:Orange-OpenSource/lfn/ci_cd/chained-ci-handson.git
   cd chained-ci-handson
   git submodule init
   git submodule update
```

The chained-ci repository can be shown as follows:

```Ascii
.
├── pod_inventory
│   ├── group_vars
│   │   └── all.yml
│   ├── host_vars
│   │   ├── existing_chained.yml
│   │   ├── ...
│   │   └── vpod1.yml
│   └── inventory
└── roles
    ├── artifact_init
    │   ├── defaults
    │   └── tasks
    ├── get_artifacts
    │   ├── defaults
    │   └── tasks
    ├── gitlab-ci-generator
    │   ├── tasks
    │   └── templates
    │── prepare
    │   ├── tasks
    ├── run-ci
    │   └── tasks
    └── trigger_myself
        └── tasks
```

The ansible roles are retrieved through the git submodules.

Create a branch corresponding to your name before running the different
exercises

```Shell
   cd chained-ci-handson
   git checkout -b <branch_name>
```

## Ex1: Build a first CI chain composed of 2 GIT projects

### 1-Chained CI chain overview

For this first exercise, a CI chain composed of 2 git projects will be built:

- chained-ci-dummy1: 1 project composed of two stages with three jobs
- chained-ci-dummy2: 1 project composed of two stages with two jobs

The branch ex1 of both projects is used for this exercise.

### 2-Analysis of each dummy project

Look at the .gitlab-ci.yml file of each dummy project to identify the stages and jobs that will be executed for each project.
A stage may be composed of different jobs that can be launched in parallel.

- [chained-ci-dummy1](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-dummy1)
- [chained-ci-dummy2](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-dummy2)

### 3-Main chained-ci file

#### a)Chain description

Each new chain scenario shall be declared in a file under the directory `pod_inventory/host_vars`.  

Note: a *pod* here has no relation with Kubernetes but is rather related to the concept of pod in OPNFV which is a set of physical or virtual machines.

Create your own chain with the name `ex1\<name\>.yml`

```yaml
---
jumphost:
scenario_steps:
  project1:
    project: chained-ci-dummy1
    branch: ex1
  project2:
    project: chained-ci-dummy2
    branch: ex1  
```

 A chain is basically composed of different projects that will be executed sequentially.

#### b)Update inventory with new chain

This new chain ex1\<name\> shall be appended in inventory file under directory "pod_inventory".  
Update the file pod_inventory/inventory:

```yaml
[chainedci]
ex1<name>
```

### 4-Check the project declaration in all.yml

The file all.yml defines each project that will call from the chain.
It is used to compile ".gitlab-ci.yml" file and defined :

- The stage to trigger the remote project
- the runner to select based on tags definition
- how to join the project

See the following example for the project "chained-ci-dummy1"

```yaml
# The stage declaration is mandatory to trigger the project
stages:
  - ...
  - chained-ci-dummy1  

# The runner is the docker or vm where the chain will be executed
runner:
  tags:
    - docker
  env_vars:
    foo: bar
  docker_proxy:
  image: registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible
  image_tag: 2.7-alpine


gitlab:
  pipeline:
    delay: 15
  base_url: https://gitlab.com
  api_url: https://gitlab.com/api/v4
  private_token: "{{ lookup('env','CI_private_token') }}"

  git_projects:
    # Each git project is declared in this section
    # - API definition to trigger the remote project using the trigger token
    # - url of the project
    # - list of parameters to set for this project
    chained-ci-dummy1:
      stage: chained-ci-dummy1
      api: https://gitlab.com/api/v4/projects/9282482
      url: https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-dummy1
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        63313931343338643036313136306365333164386136626530636539353364323362376665666563
        6162636539633565363232613036323931343561366238370a306464383935373935616134353238
        65643933643032353038626537623435346331623634383338306430326461646138626162356332
        3665613732306430650a643031633736636361653536633361326233353337623437373535396231
        66366237636530373065383664363065643064323566316163366464386661636466
      branch: "{{ lookup('env','dummy1_branch')|default('master', true) }}"
      timeout: 300
      parameters:
        ansible_verbose: "{{ lookup('env','ansible_verbose') }}"  
```

### 5-Update ".gitlab-ci.yml" file to include the new chain

The ansible role gitlab-ci-generator allows to update the `.gitlab-ci.yml` considering new chain declared in the inventory file.
Run the following command:  

```shell
ansible-playbook -i ./pod_inventory/inventory ./gitlab-ci-generator.yml
```

Edit the generated file `.gitlab-ci.yml` to understand it !

Don't forget to push the modifications to your remote personal branch:

```shell
git push --set-upstream origin <your-branch-name>
```

### 6-Execution of the created chain ex1\<name\>

- Select "pipelines" menu and then "*Run Pipeline*".
- Set `POD` as input variable key and set your chain name `ex1\<name\>` as input variable value.
- Select your personal git branch instead of the `master` branch.
- Click on "*Run Pipeline*" to execute the chain.

The execution progress is visible [here](https://orange-opensource.gitlab.io/lfn/ci_cd/chained-ci-handson) (which is [the Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) of this Gitlab project).

## Ex2: Artifacts exchange between 2 projects

For this exercise, create a new branch ex2\<name\> from former ex1 branch in both projects chained-ci-dummy1
and chained-ci-dummy2.  

This is this branch that will be called in the new chain ex2\<name\>.

### 1-Modification of chained-ci-dummy1 project

In the created branch, update the file ".gitlab-ci.yml" adding the artifact generation part in one job:

```yaml
job1-dummy1:
  <<: *chained_ci_tools
  <<: *runner
  stage: stage1
  script:
    - echo "this is a first job in stage 1"
    - echo "Artifact generation"
    - mkdir ${PWD}/vars; mkdir ${PWD}/inventory
    - date > ${PWD}/vars/date
  artifacts:
    paths:
      - vars
      - inventory
  only:
    refs:
      - triggers
```

### 2-Modification of chained-ci-dummy2 project

Update `.gitlab-ci.yml` file in the created branch to fetch the artifacts in the job "job1-dummy2":

```yaml
job1-dummy2:
  <<: *chained_ci_tools
  <<: *runner
  stage: stage1
  artifacts:
    paths:
      - vars
  script:
    - echo "this is a first job (job1) of project dummy2"
    - echo "Artifact from dummy1 project is now reachable"
    - ls -al ./vars
```

Update also the call to chained_ci_init.sh (adding -a) in the `.gitlab-ci.yml`
of the project chained-ci-dummy2.

Note: A CI/CD variable "PRIVATE_TOKEN" is created for this project to allow the  
access to produced artifacts of another project (Visible in Settings, CI/CD,  
Variables menu by only if you are maintainer on the project).

### 3-Chained-ci-handson update

Declare a new chain `ex2\<name\>` indicating the branches to select and adding
 the management of artifacts as follows:

```yaml
---
jumphost:
scenario_steps:
  project1:
    project: chained-ci-dummy1
    branch: ex2<name>
    pull_artifacts: "job1-dummy1"
  test-project1:
    project: chained-ci-dummy2
    branch: ex2<name>
    get_artifacts: project1
```

- `pull_artifacts` indicates the job's name that compiles the artifacts.  
- `get_artifacts` gives the project's name that produces the artifacts.  

You should know now how to execute this new chain

## Ex3: Initialize a GIT project to launch it from chained-ci tool

This exercise describes all the configurations to perform to execute a git project from `chained-ci-handson`.

### 1-Create the git project `dummy-test`

Within your web browser, initialize a new `dummy-test` project setting `internal` project visibility (not `private`).

#### a)Initialize the `.gitlab-ci.yml` file

Define two stages:

- `test_project1`
- `test_project2`

The content of the stages will be written after the definition of variables, common part and generic jobs.

When the `chained-ci-handson` modules are used, the following option needs to be added to fetch the last sub-modules within the runners:

```yaml
##
# Variables
##

variables:
  GIT_SUBMODULE_STRATEGY: recursive

```

Define the runners in the common part:

```yaml
.runner_tags: &runner_tags
  tags:
    - docker
```

It is not requested for this exercise, but common jobs can be defined for the different stages that will be defined in the project.  
Example of common job:

```yaml
##
# Generic Jobs
##
.chained_ci_tools: &chained_ci_tools
  before_script:
    - ./scripts/chained-ci-tools/chained-ci-init.sh -i inventory/inventory
  after_script:
    - ./scripts/chained-ci-tools/clean.sh
```

In your Linux terminal, the requested `chained-ci-tools` sub-module needs to be added to the project in the directory `scripts`.

```shell
git submodule add -b master https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-tools.git scripts/chained-ci-tools
```

Let's continue now updating `.gitlab-ci.yml`.

Define the content of the stages:

```yaml
test_project1:
  <<: *chained_ci_tools
  stage: stage1
  script:
    - echo "Tests for project1"
  <<: *runner_tags
  only:
    refs:
      - triggers
test_project2:
  <<: *chained_ci_tools
  stage: stage2
  script:
    - echo "Tests for project2"
  <<: *runner_tags
  only:
    refs:
      - triggers
```

#### b)Activate the runners

In the web browser, in the settings menu, CI/CD part, select the runners  
according to the chosen tag (here docker) and activate them (ONLY if you have
`Maintainer` role in the project, else this action is realized by default).

#### c)CI Trigger configuration

The stages declared in `.gitlab-ci.yml` file can only be launched using an api triggering (identified by the keyword "triggers").

##### CI Trigger and token creation

In the web browser, in the settings menu, CI/CD part, select the sub-menu "*Pipeline triggers*".  
Add the trigger with the description of your choice.  
A token shall be now created.

##### ANSIBLE_VAULT_PASSWORD declaration

The delivered token is not secured and will have to be encrypted using the  
variable `ANSIBLE_VAULT_PASSWORD` that shall be defined in the variable  
sub-menu of CI/CD settings menu.  
The value for this password can be a simple string like `tpchainedci`.

#### d) Additional VARIABLE declaration (OPTIONAL)

In addition to `ANSIBLE_VAULT_PASSWORD`, additional variable `PRIVATE_TOKEN` to  
allow the access to artifacts from other projects in gitlab.com or another  
gitlab may be declared.  But it is not requested in this example as no
artifacts will be managed in the scenario.

This token value is obtained selecting "*Settings*" menu of your gitlab profile  
and generating access token.

### 2-Update the project `chained-ci-handson`

- Declare the project `dummy-test` in `all.yml` to apply the learnings of exercice 1.

The trigger token is generated using the following command:

```shell
ansible-vault encrypt_string --vault-password-file vaultpassword.yml <CI trigger token from dummy-test>
```

The file `vaultpassword.yml` contains the string defined in the variable `ANSIBLE_VAULT_PASSWORD` so `tpchainedci` for example.

- Create a new chained-ci chain from previous one `ex1_<name>.yml` and name it `ex3_<name>.yml`

```yaml
---
jumphost:
scenario_steps:
  project1:
    project: chained-ci-dummy1
    branch: ex1
  project2:
    project: chained-ci-dummy2
    branch: ex1
  dummy-test:
    project: dummy-test
```

- Generate the new `.gitlab-ci.yml` file (using the `gitlab-ci-generator` role).

### 3-Execute the new chain ex3\<name\>

- Select CI/CD menu and then "*Schedules*" menu and then "*New schedule*"  
- Enter a description for this new pipeline like `ex3_<name>`  
- Set `POD` as input variable key and set your chain name `ex3_<name>` as input variable value  
- Deactivate the active button  
- Click on "*Save pipeline schedule*" to save the schedule  

Execute now your defined schedule clicking on the player button.  

As a reminder, the execution progress is visible [here](https://orange-opensource.gitlab.io/lfn/ci_cd/chained-ci-handson).
